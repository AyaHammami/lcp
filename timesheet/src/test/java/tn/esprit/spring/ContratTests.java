package tn.esprit.spring;

import static org.junit.Assert.*;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.controller.ControllerEmployeImpl;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ContratTests {

	private static final Logger l = LogManager.getLogger(ContratTests.class);
	
	@Autowired
	ControllerEmployeImpl employeControl;	
	
	@Test
	public void test() throws ParseException {

		try {
	//	 String log4jConfigFile = System.getProperty("user.dir")
	  //              + File.separator + "./src/main/resources/log4j.properties";
	    //    PropertyConfigurator.configure(log4jConfigFile);
		
	
			Employe AyaHammami = new Employe ("Aya","hammami","AyaHammami@siiconsulting.tn",true,Role.INGENIEUR);
			Employe AntoinePicciole = new Employe ("Antoine","Picciole","AntoinePicciole@siiconsulting.tn",true,Role.TECHNICIEN);

			employeControl.ajouterEmploye(AyaHammami);
			employeControl.ajouterEmploye(AntoinePicciole);
			int AyaHammamiId = employeControl.ajouterEmploye(AyaHammami);

			int AntoinePiccioleId = employeControl.ajouterEmploye(AntoinePicciole);

			//int depRhId = 2;
			//int depTelecomId= 1 ;
			
		//employeControl.affecterEmployeADepartement(KhaledKallelId, depRhId);
		//employeControl.affecterEmployeADepartement(KhaledKallelId, depTelecomId);


		//employeControl.affecterEmployeADepartement(mohamedZitouniId, depTelecomId);
		//employeControl.affecterEmployeADepartement(mohamedZitouniId, depRhId);
		
		
		SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");
		Contrat AyaHammamiContrat = new Contrat (dateFormat.parse("01/02/2015"),"CDI",2000);
		Contrat AntoinePiccioleContrat = new Contrat (dateFormat.parse("01/03/2019"),"CDI",1200);
		
		int AyaHammamiContratId = employeControl.ajouterContrat(AyaHammamiContrat); 
		int AntoinePiccioleContratId = employeControl.ajouterContrat(AntoinePiccioleContrat);
		
		employeControl.affecterContratAEmploye(AyaHammamiContratId, AyaHammamiId);
		employeControl.affecterContratAEmploye(AntoinePiccioleContratId, AntoinePiccioleId);
		
		l.info("le nom de personne qui correspond ou num de contrat " +AyaHammamiContratId + " est "+employeControl.getEmployePrenomById(AyaHammamiId));
		l.info("Merci");
		
		}
		
		
		catch (Exception e) { l.error("Erreur dans le test!!) : " + e); }
	}
		
		
		}

	

