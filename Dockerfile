# This file is a template, and might need editing before it works on your project.
FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD ./lcp/timesheet/target/reservation-service-1.0-SNAPSHOT.jar reservation-service-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/reservation-service-1.0-SNAPSHOT.jar"]
